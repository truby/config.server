#!/bin/bash

if which apt &> /dev/null; then
    alias \
        fd='fdfind' \
        ipint="ip a | grep -E 'inet .* brd' | awk '{print \$2}' | sed 's|/.*||'" \
        rm='rm -vI'
fi

if which apk &> /dev/null; then
    alias \
        rm='rm -vi'
fi

alias \
    ls='ls -h --color=auto --group-directories-first' \
    ll='ls -Al' \
    cp='cp -iv' \
    mv='mv -iv' \
    rmr='rm -rf' \
    mkd='mkdir -pv' \
    grep='grep --color=auto' \
    diff='diff --color=auto'

alias v='vim'

alias \
    fh='fd -H -E .git' \
    fcd='cd $(fh -t d | fzf || pwd)' \
    fv='v -p $(fd -t f | fzf -m)' \
    fvh='v -p $(fh -t f | fzf -m)'

cd () {
    builtin cd "$@" && ls;
}

mkcd() {
    mkdir "$@" && cd "$@";
}

alias f='nnn'

alias ipext='curl ipinfo.io/ip; echo ""'

# tmux
alias \
    tmux='tmux -2u' \
    tls='tmux ls' \
    tnew='tmux new -s' \
    tname='tls | fzf -1 | cut -d":" -f1' \
    tjoin='tmux attach -t $(tname)' \
    tcopy='tmux new-session -t $(tname)' \
    tmove='tmux rename-session -t $(tname)' \
    tkill='tmux kill-session -t $(tname)'

# Package manager
if which apt &> /dev/null; then
    alias \
        pmi='apt install' \
        pmu='apt update && apt upgrade' \
        pmr='apt remove' \
        pms='apt search'
elif which apk &> /dev/null; then
    alias \
        pmi='apk add' \
        pmu='apk update && apk upgrade' \
        pmr='apk remove' \
        pms='apk search'
fi

sss() {
    if which systemctl &> /dev/null; then
        systemctl start $1
    elif which rc-service &> /dev/null; then
        rc-service $1 start
    fi
}

sst() {
    if which systemctl &> /dev/null; then
        systemctl stop $1
    elif which rc-service &> /dev/null; then
        rc-service $1 stop
    fi
}

ssr() {
    if which systemctl &> /dev/null; then
        systemctl restart $1
    elif which rc-service &> /dev/null; then
        rc-service $1 restart
    fi
}

sse() {
    if which systemctl &> /dev/null; then
        systemctl enable $1
    elif which rc-update &> /dev/null; then
        rc-update add $1
    fi
}

ssd() {
    if which systemctl &> /dev/null; then
        systemctl disable $1
    elif which rc-update &> /dev/null; then
        rc-update del $1
    fi
}

ssdr() {
    if which systemctl &> /dev/null; then
        systemctl daemon-reload $1
    #elif which rc-service &> /dev/null; then

    fi
}

_min_prompt() {
    PS1=$3
    PS1+=$'\e[m' # clear all color first
    PS1+="$4[$3"
    if [ $(id -u) = 0 ]; then # if root, red host
        PS1+=$'\e[31m' # red
    else
        PS1+=$'\e[36m' # blue
    fi
    PS1+="$4$2$3"
    PS1+=$' \e[36m' # cyan
    PS1+=$4$1$3
    PS1+=$'\e[m'
    PS1+="$4] > "
}

_box_prompt() {
    PS1=$3
    PS1+=$'\e[m'
    PS1+="$4┌[$3"
    if [ $(id -u) = 0 ]; then  # if root, red host
        PS1+=$'\e[31m' # red
    else
        PS1+=$'\e[34m' # blue
    fi
    PS1+="$4$2$3"
    PS1+=$'\e[m'
    PS1+="$4]─[$3"
    PS1+=$'\e[36m' # cyan
    PS1+="$4$1$3"
    PS1+=$'\e[m'
    PS1+="$4]"
    PS1+=$'\n└─━ ' # ━  ╼
}

min-prompt() {
    _min_prompt '\w' $(hostname) '\[' '\]'
}

box-prompt() {
    _box_prompt '\w' $(hostname) '\[' '\]'
}

# min-prompt
box-prompt

