postgres:
  pkg.installed:
    - pkgs:
      - postgres

  file.managed:
    - user: root
    - group: root
    - mode: 644
    - names:
      - /etc/postgres/config:
        - source: salt://postgres/config

  service.running:
    - name: postgres
    - enable: True
    - watch:
      - file: /etc/postgres/config
