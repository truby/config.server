promtail:
  archive.extracted:
    - name: /usr/bin/
    - source: https://github.com/grafana/loki/releases/download/v2.2.0/promtail-linux-amd64.zip
    - source_hash: 8a0e1020381873b0111d7f6951c904e427271d4a9342adaa643c1580e23d31ec
    - overwrite: True
    - enforce_toplevel: False

  file.symlink:
    - name: /usr/bin/promtail
    - target: /usr/bin/promtail-linux-amd64

config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /etc/promtail/promtail-config.yaml:
        - source: salt://promtail/promtail-config.yaml
      - /etc/systemd/system/promtail.service:
        - source: salt://promtail/promtail.service
      - /etc/default/promtail:
        - source: salt://promtail/args

  cmd.run:
    - name: systemctl daemon-reload

  service.running:
    - name: promtail
    - enable: True
    - watch:
      - file: /usr/bin/promtail-linux-amd64
      - file: /etc/promtail/promtail-config.yaml
      - file: /etc/systemd/system/promtail.service
      - file: /etc/default/promtail
