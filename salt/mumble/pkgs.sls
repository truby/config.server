pkgs:
  pkg.installed:
    - pkgs:
      - certbot
      - mumble-server

  user.present:
    - name: mumble-server
    - groups: [mumble-server, root]

  cmd.run:
    - name: grep -oE 'SuperUser.*$' /var/log/mumble-server/mumble-server.log | sed "s/'//g"
