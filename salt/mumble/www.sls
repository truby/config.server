copy_dir:
  file.recurse:
    - name: /var/www/mumble
    - source: 'salt://mumble/www'
    - clean: True
    - user: root
    - group: root
    - dir_mode: 755
    - file_mode: 644

copy_config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /etc/nginx/sites-available/mumble:
        - source: salt://mumble/mumble.nginx.conf

replace_token:
  file.replace:
    - name: /etc/nginx/sites-available/mumble
    - pattern: mumble_domain
    - repl: {{ pillar['mumble_values']['mumble_domain'] }}
    - backup: False

symlink:
  file.symlink:
    - name: /etc/nginx/sites-enabled/mumble
    - target: /etc/nginx/sites-available/mumble

restart_nginx:
  service.running:
    - name: nginx
    - enable: True
    - watch:
      - file: /etc/nginx/sites-enabled/*
