include:
  - mumble.pkgs

config_files:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - names:
      - /etc/mumble-server.ini:
        - source: salt://mumble/mumble-server.ini

{% for key, value in pillar['mumble_values'].items() %}
{{ key }}:
  file.replace:
    - name: /etc/mumble-server.ini
    - pattern: {{ key }}
    - repl: {{ value }}
    - backup: False
{% endfor %}

restart_mumble:
  service.running:
    - name: mumble-server
    - enable: True
    - watch:
      - file: /etc/mumble/*
