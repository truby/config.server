prometheus:
  pkg.installed:
    - pkgs:
      - prometheus

config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /etc/default/prometheus:
        - source: salt://prometheus/args
      - /etc/prometheus/prometheus.yml:
        - source: salt://prometheus/prometheus.yml
      - /etc/prometheus/alert_rules/rules.yml:
        - source: salt://prometheus/rules.yml

prometheus_service:
  service.running:
    - name: prometheus
    - enable: True
    - watch:
      - file: /etc/default/prometheus
      - file: /etc/prometheus/prometheus.yml
      - file: /etc/prometheus/alert_rules/*.yml
