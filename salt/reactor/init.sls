config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - names:
      - /etc/salt/master.d/reactor.conf:
        - source: salt://reactor/reactor.conf

  service.running:
    - name: salt-master
    - enable: True
    - watch:
      - file: /etc/salt/master.d/reactor.conf
