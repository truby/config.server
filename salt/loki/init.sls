loki:
  archive.extracted:
    - name: /usr/bin/
    - source: https://github.com/grafana/loki/releases/download/v2.2.0/loki-linux-amd64.zip
    - source_hash: 3d06f27e1e2ac5fa15261918bd14bb5da24988981634ea09855f8c67a3ba9ae3
    - overwrite: True
    - enforce_toplevel: False

  file.symlink:
    - name: /usr/bin/loki
    - target: /usr/bin/loki-linux-amd64

config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /etc/loki/loki-config.yaml:
        - source: salt://loki/loki-config.yaml
      - /etc/systemd/system/loki.service:
        - source: salt://loki/loki.service
      - /etc/default/loki:
        - source: salt://loki/args

  cmd.run:
    - name: systemctl daemon-reload

  service.running:
    - name: loki
    - enable: True
    - watch:
      - file: /usr/bin/loki-linux-amd64
      - file: /etc/loki/loki-config.yaml
      - file: /etc/systemd/system/loki.service
      - file: /etc/default/loki
