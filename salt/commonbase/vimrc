syntax enable

" Show line numbers
set number

" Relative line numbers
set number relativenumber

" Replace tabs with 4 spaces
set expandtab
set shiftwidth=4
set tabstop=4

set history=100

filetype plugin on
filetype indent on

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" The 'hidden' option allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
set hidden

" turn off backups, since most stuff is in source control
set nobackup
set nowritebackup
set nowb
set noswapfile
set updatetime=100

" set to auto read when a file is changed from the outside
set autoread

" when using %s/ find and replace, the last g is implied, since
" the default behavior is to replace the first occurence
set gdefault

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Better command-line completion
set wildmenu

" Better auto completion
set wildmode=longest,list,full

" ignore binary files
set wildignore+=*/tmp/*,*.o,*.so,*.swp,*.zip
 
" Show partial commands in the last line of the screen
set showcmd

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
set hlsearch
set incsearch
set lazyredraw

" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent
 
" Stop certain movements from always going to the first character of a line.
" While this behaviour deviates from that of Vi, it does what most users
" coming from other editors would expect.
set nostartofline

" Always show the status line
set laststatus=2

" enable use of mouse for all modes
set mouse=a
if !has('nvim')
    set ttymouse=sgr
endif

" More natural split opening
set splitbelow splitright

" We show this on status line
set noshowmode

set shell=bash

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

colorscheme truby

inoremap <C-@> <Esc>

" File  browser
nnoremap <C-n> :e.<CR>

" Leader
let mapleader = " "
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>z :wq<CR>
nnoremap <leader>n :noh<CR>
nnoremap <leader>r :%s/

" Move lines up and down
nnoremap <leader>j mz:m+<CR>
nnoremap <leader>k mz:m-2<CR>

" Splits
nnoremap <leader>h :sp<CR>
nnoremap <leader>v :vsp<CR>
nnoremap <leader>- :sp<CR>
nnoremap <leader>\ :vsp<CR>

" Spell checking
nnoremap <leader>S :setlocal spell!<CR>

" Makes j and k behave on multilines
nnoremap j gj
nnoremap k gk

" tab follows brackets because % is too hard
nnoremap <tab> %

" Easy navigating between split panes
nnoremap <C-j> <C-W><C-j>
nnoremap <C-k> <C-W><C-k>
nnoremap <C-l> <C-W><C-l>
nnoremap <C-h> <C-W><C-h>

nnoremap <C-e> :tabprevious<CR>
nnoremap <C-r> :tabnext<CR>
nnoremap <silent> <C-[> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <C-]> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" Completion addon agnostic tab / shift tab for menus
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Quick line duplication
nnoremap <C-g> yyp

" Highlight and copy in visual mode
vnoremap <C-c> :w !xsel -b<CR><CR>
" nnoremap <C-v> :r !xsel -b<CR>

" Visual mode indentation
vnoremap > >gv
vnoremap < <gv
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

" Visual mode moving lines up and down
vnoremap <S-Up> :move '<-2<CR>gv-gv
vnoremap <S-Down> :move '>+1<CR>gv-gv

" force vim to behave when starting up in ssh
nnoremap <esc>^[ <esc>^[

" Format the status line
set statusline=
" cut at start
" set statusline+=%<

function! ModeFormatted() abort
    let l:mode = mode()
    if l:mode == 'i'
        return ' I '
    elseif l:mode == 'v'
        return ' V '
    else
        return ''
    endif
endfunction

set statusline+=%#User7#%{ModeFormatted()}%#User8#

" flags
set statusline+=%#User9#
set statusline+=%h%m%r%w
set statusline+=%*

" relative filename
set statusline+=\ %f\  

" filetype
set statusline+=%y\  

" right align
set statusline+=%#LineNr#%=%#StatusLine#

" line number / lines
set statusline+=\ %l\/%L\ 

" line number and column
" set statusline+=\ %l,%c\  
" Percentage through file
" set statusline+=%P\  

