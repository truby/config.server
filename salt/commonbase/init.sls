{%- set user = 'nodeuser' %}

install:
  pkg.installed:
    - pkgs:
      - fail2ban
      - fd-find
      - fzf
      - htop
      - jq
      - nginx
      - nnn
      - tmux
      - prometheus-node-exporter
      - prometheus-nginx-exporter

  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /root/.bashrc:
        - source: salt://commonbase/bashrc
      - /root/.tmux.conf:
        - source: salt://commonbase/tmux.conf
      - /root/.config/htop/htoprc:
        - source: salt://commonbase/htoprc
      - /root/.vim/vimrc:
        - source: salt://commonbase/vimrc
      - /root/.vim/colors/truby.vim:
        - source: salt://commonbase/truby.vim
      - /etc/nginx/sites-available/stub_status:
        - source: salt://commonbase/stub_status
      - /var/www/html/index.html:
        - source: salt://commonbase/index.html
      - /etc/ssh/sshd_config:
        - source: salt://commonbase/sshd_config

create_user:
  user.present:
    - name: {{ user }}
    - usergroup: True
    - groups: [root, sudo]
    - password: $5$xnRhUxcJlTFm$43Edz52i/zAwm64Nmp12VPnPUFfrdd4Vj9Sr7QXDPe/
    - shell: /bin/bash

  file.managed:
    - user: {{ user }}
    - group: {{ user }}
    - mode: 644
    - makedirs: True
    - names:
      - /home/{{ user }}/.bashrc:
        - source: salt://commonbase/bashrc
      - /home/{{ user }}/.tmux.conf:
        - source: salt://commonbase/tmux.conf
      - /home/{{ user }}/.config/htop/htoprc:
        - source: salt://commonbase/htoprc
      - /home/{{ user }}/.vim/vimrc:
        - source: salt://commonbase/vimrc
      - /home/{{ user }}/.vim/colors/truby.vim:
        - source: salt://commonbase/truby.vim

copy_ssh_keys:
  file.managed:
    - user: {{ user }}
    - group: {{ user }}
    - mode: 600
    - makedirs: True
    - names:
      - /home/{{ user }}/.ssh/authorized_keys:
        - source: /root/.ssh/authorized_keys

remove_default_nginx_files:
  file.absent:
    - name: /var/www/html/index.nginx-debian.html

symlink:
  file.symlink:
    - name: /etc/nginx/sites-enabled/stub_status
    - target: /etc/nginx/sites-available/stub_status

ssh_service:
  service.running:
    - name: ssh
    - enable: True
    - watch:
      - file: /etc/ssh/sshd_config

nginx_service:
  service.running:
    - name: nginx
    - enable: True
    - watch:
      - file: /etc/nginx/sites-enabled/*

nginx_exporter_service:
  service.running:
    - name: prometheus-nginx-exporter
    - enable: True
