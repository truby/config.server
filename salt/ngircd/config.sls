include:
  - ngircd.pkgs

config_files:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - names:
      - /etc/ngircd/ngircd.conf:
        - source: salt://ngircd/ngircd.conf
      - /etc/ngircd/ngircd.motd:
        - source: salt://ngircd/ngircd.motd

{% for key, value in pillar['irc_values'].items() %}
{{ key }}:
  file.replace:
    - name: /etc/ngircd/ngircd.conf
    - pattern: {{ key }}
    - repl: {{ value }}
    - backup: False
{% endfor %}

restart_ngircd:
  service.running:
    - name: ngircd
    - enable: True
    - watch:
      - file: /etc/ngircd/*
