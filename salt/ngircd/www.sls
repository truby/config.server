copy_dir:
  file.recurse:
    - name: /var/www/irc
    - source: 'salt://ngircd/www'
    - clean: True
    - user: root
    - group: root
    - dir_mode: 755
    - file_mode: 644

copy_config:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - names:
      - /etc/nginx/sites-available/irc:
        - source: salt://ngircd/irc.nginx.conf

replace_token:
  file.replace:
    - name: /etc/nginx/sites-available/irc
    - pattern: irc_domain
    - repl: {{ pillar['irc_values']['irc_domain'] }}
    - backup: False

symlink:
  file.symlink:
    - name: /etc/nginx/sites-enabled/irc
    - target: /etc/nginx/sites-available/irc

restart_nginx:
  service.running:
    - name: nginx
    - enable: True
    - watch:
      - file: /etc/nginx/sites-enabled/*
