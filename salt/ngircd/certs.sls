include:
  - ngircd.pkgs

get_certs:
  cmd.run:
    - name: certbot certonly --webroot --agree-tos -n -m 'do.not@email.me' -d {{ pillar['irc_values']['irc_domain'] }} -w /var/www/html/
    - unless: "[ -d /etc/letsencrypt/archive/{{ pillar['irc_values']['irc_domain'] }} ]"
    - creates: "/etc/letsencrypt/archive/{{ pillar['irc_values']['irc_domain'] }}"

copy_script:
  file.managed:
    - user: root
    - group: root
    - mode: 755
    - names:
      - /root/renew_certs:
        - source: salt://scripts/renew_certs

add_cronjob:
  cron.present:
    - identifier: renew_certs 
    - name: /root/renew_certs
    - minute: 0
    - hour: 8
    - dayweek: 3

fix_archive_permissions:
  file.directory:
    - user: root
    - group: root
    - dir_mode: 750
    - name: /etc/letsencrypt/archive

fix_live_permissions:
  file.directory:
    - user: root
    - group: root
    - dir_mode: 750
    - name: /etc/letsencrypt/live

fix_privkey_permissions:
  cmd.run:
    - name: chmod 640 /etc/letsencrypt/archive/{{ pillar['irc_values']['irc_domain'] }}/privkey*.pem
