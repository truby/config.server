pkgs:
  pkg.installed:
    - pkgs:
      - certbot
      - ngircd

  user.present:
    - name: irc
    - groups: [irc, root]
